function Contains(stroke, char)
{
    if(stroke.indexOf(char) != -1)
        return char;
    return "";
}

function IsPalindromeStr(stroke)
{
    let nums = "1234567890";

    let res = "";

    stroke.array.forEach(element => {
        res += Contains(nums, element)
    });

    if(stroke != res)
        console.log("String value");

    return IsPalindrome(parseInt(res));
}

// n - число
function IsPalindrome(n)
{
    return n + (((n+="").split("").reverse().join("") == n) ? " is a palindrome" : " is not a palindrome");
}
    
// ======== test =============
console.log(IsPalindrome(12321)); // +
console.log(IsPalindrome(121)); // +
console.log(IsPalindrome(9889)); // +
console.log(IsPalindrome(123456789)); // - 
console.log(IsPalindrome(1)); // +
